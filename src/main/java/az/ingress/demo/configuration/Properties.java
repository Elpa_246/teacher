package az.ingress.demo.configuration;

import az.ingress.demo.model.Car;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("car")
@Data
public class Properties {
    List<String> model;
}
