package az.ingress.demo;

import az.ingress.demo.configuration.Properties;
import az.ingress.demo.controller.CarController;
import az.ingress.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class DemoApplication implements CommandLineRunner {
	private final Properties properties;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		for (String model: properties.getModel()){
			System.out.println(model);
		}

	}
}
