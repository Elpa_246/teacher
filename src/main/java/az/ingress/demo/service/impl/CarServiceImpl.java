package az.ingress.demo.service.impl;

import az.ingress.demo.model.Car;
import az.ingress.demo.repository.CarRepository;
import az.ingress.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    @Override
    public Car getCarById(int id) {
        return carRepository.findById(id).get();
    }

    @Override
    public String saveCar(Car car) {
        return "Saving"+ carRepository.save(car);
    }

    @Override
    public void save(Car car) {

    }

    @Override
    public Integer updateCar(int id, Car car) {
        Car car1 = carRepository.findById(id).get();
        car1.setName(car.getName());
        car1.setId(car.getId());
        car1.setColor(car.getColor());
        car1.setModel(car.getModel());
        car1.setEngineType(car.getEngineType());
        carRepository.save(car1);
        return id;
    }


    @Override
    public void deleteCar(int id) {

    }

    @Override
    public void delete(Car car) {

    }
}
