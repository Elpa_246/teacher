package az.ingress.demo.service;

import az.ingress.demo.model.Car;

public interface CarService {
    Car getCarById(int id);

    String saveCar(Car car);

    void save(Car car);
    Integer updateCar (int id, Car car);


    void deleteCar(int id);

    void delete(Car car);
}